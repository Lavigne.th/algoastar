﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = "Grille.xlsx";
            FileInfo fileInfo = new FileInfo(filePath);

            ExcelPackage package = new ExcelPackage(fileInfo);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();

            // get number of rows and columns in the sheet
            int rows = worksheet.Dimension.Rows; // 20
            int columns = worksheet.Dimension.Columns; // 7

            Cell[,] world = new Cell[columns, rows];

            Cell destination = null;
            Cell origin = null;

            // loop through the worksheet rows and columns
            for (int y = 1; y <= rows; y++)
            {
                for (int x = 1; x <= columns; x++)
                {
                    string content = worksheet.Cells[y, x].Value.ToString();
                    if (content == "S")
                    {
                        destination = new Cell(false, 0, x - 1, y - 1);
                        world[x - 1, y - 1] = destination;
                    } else if(content == "E")
                    {
                        origin = new Cell(false, 0, x - 1, y - 1);
                        world[x - 1, y - 1] = origin;
                    } else
                    {
                        bool isSolid = content == "X";
                        int weight = content == "X" ? 0 : int.Parse(content);

                        world[x - 1, y - 1] = new Cell(isSolid, weight, x - 1, y - 1);

                    }
                }
            }

            PathUtils utils = new PathUtils(world, columns, rows);
            List<Cell> path = utils.FindPath(origin, destination);
            PrintPath(path);

            Console.Read();
        }

        public static void PrintPath(List<Cell> cells)
        {
            int totalWeight = 0;
            foreach(Cell cell in cells)
            {
                totalWeight += cell.TotalCost;
                Console.WriteLine((char)(cell.X + 'a') + " " + (cell.Y + 1));
            }
            Console.WriteLine("Poids total : " + totalWeight);
        }

    }
}
