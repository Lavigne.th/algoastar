﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding
{
    class PathUtils
    {
        public Cell[,] World;

        public int Width;

        public int Height;

        public PathUtils(Cell[,] world, int width, int height)
        {
            World = world;
            Width = width;
            Height = height;
        }

        public List<Cell> FindPath(Cell origin, Cell destination)
        {
            List<Cell> open = new List<Cell>();
            List<Cell> closed = new List<Cell>();

            open.Add(origin);

            while(open.Count > 0)
            {
                int minCost = open.Min(c => c.TotalCost);
                Cell currentNode = open.Find(node => node.TotalCost == minCost);  
                open.Remove(currentNode);
                closed.Add(currentNode);

                if (currentNode.GetDistance(destination) == 10)
                {
                    destination.Parent = currentNode;
                    return AssemblePath(destination);
                }
                   

                foreach(Cell adjacent in GetAdjacenteCells(currentNode))
                {
                    if (adjacent.IsSolid || closed.Contains(adjacent))
                        continue;

                    int distanceOrigin = currentNode.DistanceOrigin + 10;
                    int totalCost = distanceOrigin + adjacent.GetDistance(destination) + 10 * adjacent.Weight;
                    if(!open.Contains(adjacent) || adjacent.TotalCost > totalCost)
                    {
                        adjacent.DistanceOrigin = distanceOrigin;
                        adjacent.TotalCost = totalCost;
                        adjacent.Parent = currentNode;
                    }

                    if (!open.Contains(adjacent))
                        open.Add(adjacent);
                }
            }

            return null;
        }


        public List<Cell> AssemblePath(Cell destination)
        {
            List<Cell> path = new List<Cell>();

            Cell currentNode = destination;
            while(currentNode != null)
            {
                path.Add(currentNode);
                currentNode = currentNode.Parent;
            }
            return path.Reverse<Cell>().ToList();
        }


        private List<Cell> GetAdjacenteCells(Cell node)
        {
            List<Cell> adjacent = new List<Cell>();

            if (node.X + 1 < Width)
                adjacent.Add(World[node.X + 1, node.Y]);
            if (node.X - 1 >= 0)
                adjacent.Add(World[node.X - 1, node.Y]);
            if (node.Y + 1 < Height)
                adjacent.Add(World[node.X, node.Y + 1]);
            if (node.Y - 1 >= 0)
                adjacent.Add(World[node.X, node.Y - 1]);

            return adjacent;
        }
    }
}
