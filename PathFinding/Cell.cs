﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding
{
    class Cell
    {
        public bool IsSolid;

        public int Weight;

        public int X;

        public int Y;

        public int TotalCost;

        public int DistanceOrigin;

        public Cell Parent;

        public Cell(bool isSolid, int weight, int x, int y)
        {
            IsSolid = isSolid;
            Weight = weight;
            X = x;
            Y = y;
        }

        public int GetDistance(Cell cell)
        {
            int xDist = Math.Abs(cell.X - X);
            int yDist = Math.Abs(cell.Y - Y);
            return (xDist + yDist) * 10;
        }
    }
}
